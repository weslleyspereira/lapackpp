#define LAPACK_srscl LAPACK_GLOBAL(srscl,SRSCL)
void LAPACK_srscl(
    lapack_int const* n, float const* sa, float* SX, lapack_int const* incx
);

#define LAPACK_drscl LAPACK_GLOBAL(drscl,DRSCL)
void LAPACK_drscl(
    lapack_int const* n, double const* sa, double* SX, lapack_int const* incx
);

#define LAPACK_csrscl LAPACK_GLOBAL(csrscl,CSRSCL)
void LAPACK_csrscl(
    lapack_int const* n, float const* sa, lapack_complex_float* SX, lapack_int const* incx
);

#define LAPACK_zdrscl LAPACK_GLOBAL(zdrscl,ZDRSCL)
void LAPACK_zdrscl(
    lapack_int const* n, double const* sa, lapack_complex_double* SX, lapack_int const* incx
);

