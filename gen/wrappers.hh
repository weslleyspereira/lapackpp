#ifndef LAPACK_WRAPPERS_HH
#define LAPACK_WRAPPERS_HH

#include "lapack/util.hh"

namespace lapack {

// -----------------------------------------------------------------------------
void rscl(
    int64_t n, float sa,
    float* SX, int64_t incx );

void rscl(
    int64_t n, double sa,
    double* SX, int64_t incx );

void rscl(
    int64_t n, float sa,
    std::complex<float>* SX, int64_t incx );

void rscl(
    int64_t n, double sa,
    std::complex<double>* SX, int64_t incx );


}  // namespace lapack

#endif // LAPACK_WRAPPERS_HH
