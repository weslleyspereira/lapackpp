// Copyright (c) 2017-2022, University of Tennessee. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause
// This program is free software: you can redistribute it and/or modify it under
// the terms of the BSD 3-Clause license. See the accompanying LICENSE file.

#include "lapack.hh"
#include "lapack/fortran.h"

#include <vector>

namespace lapack {

using blas::max;
using blas::min;
using blas::real;

// -----------------------------------------------------------------------------
/// @ingroup realOTHERauxiliary
void rscl(
    int64_t n, float sa,
    float* SX, int64_t incx )
{
    // check for overflow
    if (sizeof(int64_t) > sizeof(lapack_int)) {
        lapack_error_if( std::abs(n) > std::numeric_limits<lapack_int>::max() );
        lapack_error_if( std::abs(incx) > std::numeric_limits<lapack_int>::max() );
    }
    lapack_int n_ = (lapack_int) n;
    lapack_int incx_ = (lapack_int) incx;

    LAPACK_srscl(
        &n_, &sa,
        SX, &incx_ );
}

// -----------------------------------------------------------------------------
/// @ingroup oubleOTHERauxiliary
void rscl(
    int64_t n, double sa,
    double* SX, int64_t incx )
{
    // check for overflow
    if (sizeof(int64_t) > sizeof(lapack_int)) {
        lapack_error_if( std::abs(n) > std::numeric_limits<lapack_int>::max() );
        lapack_error_if( std::abs(incx) > std::numeric_limits<lapack_int>::max() );
    }
    lapack_int n_ = (lapack_int) n;
    lapack_int incx_ = (lapack_int) incx;

    LAPACK_drscl(
        &n_, &sa,
        SX, &incx_ );
}

// -----------------------------------------------------------------------------
/// @ingroup omplexOTHERauxiliary
void rscl(
    int64_t n, float sa,
    std::complex<float>* SX, int64_t incx )
{
    // check for overflow
    if (sizeof(int64_t) > sizeof(lapack_int)) {
        lapack_error_if( std::abs(n) > std::numeric_limits<lapack_int>::max() );
        lapack_error_if( std::abs(incx) > std::numeric_limits<lapack_int>::max() );
    }
    lapack_int n_ = (lapack_int) n;
    lapack_int incx_ = (lapack_int) incx;

    LAPACK_csrscl(
        &n_, &sa,
        (lapack_complex_float*) SX, &incx_ );
}

// -----------------------------------------------------------------------------
/// Multiplies an n-element complex vector x by the real scalar
/// 1/a. This is done without overflow or underflow as long as
/// the final result x/a does not overflow or underflow.
///
/// Overloaded versions are available for
/// `float`, `double`, `std::complex<float>`, and `std::complex<double>`.
///
/// @param[in] n
///     The number of components of the vector x.
///
/// @param[in] sa
///     The scalar a which is used to divide each component of x.
///     sa must be >= 0, or the subroutine will divide by zero.
///
/// @param[in,out] SX
///     The vector SX of length 1+(n-1)*abs(incx).
///         (1+(n-1)*abs(incx))
///     The n-element vector x.
///
/// @param[in] incx
///     The increment between successive values of the vector SX.
///     > 0: SX(1) = X(1) and SX(1+(i-1)*incx) = x(i), 1< i<= n
///
/// @ingroup omplex16OTHERauxiliary
void rscl(
    int64_t n, double sa,
    std::complex<double>* SX, int64_t incx )
{
    // check for overflow
    if (sizeof(int64_t) > sizeof(lapack_int)) {
        lapack_error_if( std::abs(n) > std::numeric_limits<lapack_int>::max() );
        lapack_error_if( std::abs(incx) > std::numeric_limits<lapack_int>::max() );
    }
    lapack_int n_ = (lapack_int) n;
    lapack_int incx_ = (lapack_int) incx;

    LAPACK_zdrscl(
        &n_, &sa,
        (lapack_complex_double*) SX, &incx_ );
}

}  // namespace lapack
