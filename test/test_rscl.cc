// Copyright (c) 2017-2022, University of Tennessee. All rights reserved.
// SPDX-License-Identifier: BSD-3-Clause
// This program is free software: you can redistribute it and/or modify it under
// the terms of the BSD 3-Clause license. See the accompanying LICENSE file.

#include "test.hh"
#include "lapack.hh"
#include "lapack/flops.hh"
#include "print_matrix.hh"
#include "error.hh"
#include "lapacke_wrappers.hh"

#include <vector>

//------------------------------------------------------------------------------
// Simple overloaded wrappers around LAPACKE (assuming routines in LAPACKE).
// These should go in test/lapacke_wrappers.hh.
inline lapack_int LAPACKE_rscl(
    lapack_int n, float sa,
    float* SX, lapack_int incx )
{
    return LAPACKE_srscl(
        LAPACK_COL_MAJOR, n, sa,
        SX, incx );
}

inline lapack_int LAPACKE_rscl(
    lapack_int n, double sa,
    double* SX, lapack_int incx )
{
    return LAPACKE_drscl(
        LAPACK_COL_MAJOR, n, sa,
        SX, incx );
}

inline lapack_int LAPACKE_rscl(
    lapack_int n, float sa,
    std::complex<float>* SX, lapack_int incx )
{
    return LAPACKE_csrscl(
        LAPACK_COL_MAJOR, n, sa,
        (lapack_complex_float*) SX, incx );
}

inline lapack_int LAPACKE_rscl(
    lapack_int n, double sa,
    std::complex<double>* SX, lapack_int incx )
{
    return LAPACKE_zdrscl(
        LAPACK_COL_MAJOR, n, sa,
        (lapack_complex_double*) SX, incx );
}

//------------------------------------------------------------------------------
template< typename scalar_t >
void test_rscl_work( Params& params, bool run )
{
    using real_t = blas::real_type< scalar_t >;
    typedef long long lld;

    // get & mark input values
    int64_t n = params.dim.n();
    real_t sa = params.sa();
    int64_t align = params.align();

    // mark non-standard output values
    params.ref_time();
    params.ref_gflops();
    params.gflops();

    if (! run)
        return;

    //---------- setup
    int64_t incx;  // todo value
    size_t size_SX = (size_t) (1+(n-1)*abs(incx));

    std::vector< scalar_t > SX_tst( size_SX );
    std::vector< scalar_t > SX_ref( size_SX );

    int64_t idist = 1;
    int64_t iseed[4] = { 0, 1, 2, 3 };
    lapack::larnv( idist, iseed, SX_tst.size(), &SX_tst[0] );
    SX_ref = SX_tst;

    //---------- run test
    testsweeper::flush_cache( params.cache() );
    double time = testsweeper::get_wtime();
    int64_t info_tst = lapack::rscl( n, sa, &SX_tst[0], incx );
    time = testsweeper::get_wtime() - time;
    if (info_tst != 0) {
        fprintf( stderr, "lapack::rscl returned error %lld\n", (lld) info_tst );
    }

    params.time() = time;
    double gflop = lapack::Gflop< scalar_t >::rscl( n, sa );
    params.gflops() = gflop / time;

    if (params.ref() == 'y' || params.check() == 'y') {
        //---------- run reference
        testsweeper::flush_cache( params.cache() );
        time = testsweeper::get_wtime();
        int64_t info_ref = LAPACKE_rscl( n, sa, &SX_ref[0], incx );
        time = testsweeper::get_wtime() - time;
        if (info_ref != 0) {
            fprintf( stderr, "LAPACKE_rscl returned error %lld\n", (lld) info_ref );
        }

        params.ref_time() = time;
        params.ref_gflops() = gflop / time;

        //---------- check error compared to reference
        real_t error = 0;
        if (info_tst != info_ref) {
            error = 1;
        }
        error += abs_error( SX_tst, SX_ref );
        params.error() = error;
        params.okay() = (error == 0);  // expect lapackpp == lapacke
    }
}

//------------------------------------------------------------------------------
void test_rscl( Params& params, bool run )
{
    switch (params.datatype()) {
        case testsweeper::DataType::Integer:
            throw std::exception();
            break;

        case testsweeper::DataType::Single:
            test_rscl_work< float >( params, run );
            break;

        case testsweeper::DataType::Double:
            test_rscl_work< double >( params, run );
            break;

        case testsweeper::DataType::SingleComplex:
            test_rscl_work< std::complex<float> >( params, run );
            break;

        case testsweeper::DataType::DoubleComplex:
            test_rscl_work< std::complex<double> >( params, run );
            break;
    }
}
